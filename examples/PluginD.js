(function ($) {
    var parent = jc.Plugin;
    var prototype = jc.makePluginPrototype(
        'demo.PluginD',
        'demo_pluginD',
        parent
    );
    
    prototype.source={
        "method":"get",
        "url":"server.php?tpl=template1",
        "dataType":"json"
    };
    
    prototype.init = function () {
        var self = this;
        
        self.elPage = self.find('page');
        self.elPage.click(function () {
            self.loadPage($(this).text())
        });
        
        parent.prototype.init.call(self);
    };
    
    prototype.loadPage=function(pageIndex){
        var self = this;
        self.reload({
            source:{
                data:{
                    page:pageIndex
                }
            }
        });
    }
    
})(jQuery);